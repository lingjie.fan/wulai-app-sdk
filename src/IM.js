import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  SafeAreaView,
  StatusBar,
  Platform,
  KeyboardAvoidingView,
  TouchableOpacity,
  View,
  Image,
  ListView,
  RefreshControl,
  TextInput,
  Keyboard,
  Dimensions
} from 'react-native';
import Svg, {Circle, G, Path} from 'react-native-svg';

const COLOR = {
    HeaderFontColor: '#ffffff',
    HeaderBackground: '#38b2a6',
    TimeFontColor: '#aeaeae',
    UserFontColor: '#ffffff',
    UserBackground: '#38b2a6',
    BotFontColor: '#000000',
    BotBackground: '#f5f5f5',
    BorderColor: '#f5f5f5',
    ButtonFontColor: '#ffffff',
    ButtonBackground: '#38b2a6',
    FooterFontColor: '#aeaeae',
    FooterBackground: '#f8f8f6'
};

const screenW = Dimensions.get('window').width;
const screenH = Dimensions.get('window').height;

// iPhoneX
const X_WIDTH = 375;
const X_HEIGHT = 812;


function isIphone() {
    return (Platform.OS === 'ios')
}

function isIphoneX() {
    return (
        Platform.OS === 'ios' &&
        ((screenH === X_HEIGHT && screenW === X_WIDTH) ||
            (screenH === X_WIDTH && screenW === X_HEIGHT))
    )
}


class Header extends Component {
  render() {
    let closeHandler = this.props.closeHandler;
    return (
      <View style={header_styles.container}>
        <Svg style={header_styles.logo}>
        <G stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" transform="translate(-12.000000, -14.000000)">
            <G transform="translate(12.000000, 14.000000)">
                <Circle id="Oval" fill="#FFFFFF" cx="16" cy="16" r="16"></Circle>
                <Path d="M6.65,12.2419286 C5.64192857,12.8830714 5,14.075 5,15.5419286 C5,17.65 6.28307143,19.3919286 8.39192857,19.3919286 C8.85,19.3919286 9.21692857,19.025 9.21692857,18.5669286 L9.21692857,12.6080714 C9.21692857,12.2419286 8.94192857,11.875 8.575,11.7830714 C9.85807143,8.75807143 12.7,6.65 16,6.65 C19.3,6.65 22.1419286,8.75807143 23.425,11.7830714 C23.0580714,11.875 22.7830714,12.15 22.7830714,12.6080714 L22.7830714,18.5669286 C22.7830714,19.025 23.15,19.3919286 23.6080714,19.3919286 C23.7919286,19.3919286 23.975,19.3919286 24.1580714,19.3 C23.8830714,20.8580714 23.2419286,22.05 22.5080714,22.875 C22.05,23.425 21.6830714,23.7919286 21.3169286,23.975 C21.1788877,24.0435485 21.0545357,24.136746 20.95,24.25 C20.8580714,24.25 20.8580714,24.25 20.8580714,24.3419286 L19.3,24.3419286 C19.025,23.425 18.0169286,22.7830714 16.825,22.7830714 C15.45,22.7830714 14.2580714,23.7 14.2580714,24.8919286 C14.2580714,26.0830714 15.3580714,27 16.825,27  C18.0169286,27 18.9330714,26.3580714 19.3,25.4419286 L20.95,25.4419286 C21.225,25.4419286 21.3169286,25.35 21.5,25.2580714 C21.6830714,25.1669286 21.8669286,25.075 22.05,24.8919286 C22.5080714,24.6169286 22.9669286,24.1580714 23.5169286,23.6080714 C24.4330714,22.5080714 25.2580714,20.95 25.4419286,18.8419286 C26.45,18.2 27,17.0080714 27,15.6330714 C27,14.1669286 26.3580714,12.975 25.35,12.3330714 C24.1580714,8.025 20.4,5 16,5 C11.6,5 7.84192857,8.025 6.65,12.2419286 Z"  fill="#38B2A6" fill-rule="nonzero"/>
            </G>
            </G>
        </Svg>
        <Text style={header_styles.text}>自助客户</Text>
        <TouchableOpacity style={header_styles.close} onPress={closeHandler}>
        <Svg style={header_styles.icon}>
          <G stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" transform="translate(-346.000000, -23.000000)">
            <G fill="#F8F8F6" stroke="#F8F8F6" stroke-width="0.5">
              <Path d="M358.913754,35.4987188 C359.028463,35.6134278 359.028463,35.7989025 358.913754,35.9141821 C358.799045,36.0294618 358.614141,36.0288911 358.498861,35.9141821 L353.00025,30.4155703 L347.501067,35.9141821 C347.386358,36.0288911 347.200883,36.0288911 347.085604,35.9141821 C346.971465,35.7994732 346.971465,35.6139985 347.085604,35.4987188 L352.584786,30.0001069 L347.085604,24.5014951 C346.971465,24.3862154 346.971465,24.2013114 347.085604,24.0860317 C347.200313,23.9713228 347.385787,23.9713228 347.501067,24.0860317 L353.00025,29.5846436 L358.498861,24.0860317 C358.61357,23.9713228 358.798474,23.9713228 358.913754,24.0860317 C359.029034,24.2007407 359.028463,24.3856447 358.913754,24.5014951 L353.415142,30.0001069 L358.913754,35.4987188 Z"/>
            </G>
          </G>
        </Svg>
        </TouchableOpacity>
      </View>
    )
  }
};

const header_styles = StyleSheet.create({
  container: {
    height: 60,
    backgroundColor: COLOR.HeaderBackground,
    flexDirection: 'row',
    width: "100%"
  },
  logo: {
    height: 60,
    width: 40,
    marginTop: 14,
    marginLeft: 12
  },
  text: {
    marginLeft: 1,
    fontSize: 14,
    fontWeight: "500",
    lineHeight: 60,
    color: COLOR.HeaderFontColor,
    textAlign: 'center'
  },
  close: {
    right: 0,
    position: 'absolute'
  },
  icon: {
    marginTop: 24,
    marginLeft: 10,
    height: 30,
    width: 30
  }
});


class Footer extends Component {
  render() {
    if(isIphoneX()){
      setTimeout(() => {
        if(FOOTER_HEIGHT == 34){
          FOOTER_HEIGHT = 44;
        } else {
          FOOTER_HEIGHT = 34;
        }
      }, 100);
    }
    return (
      <View style={[footer_styles.container, {height: FOOTER_HEIGHT}]}>
        <Text style={footer_styles.text}>来也提供技术服务</Text>
      </View>
    )
  }
}

FOOTER_HEIGHT = (isIphoneX()?44:34);

const footer_styles = StyleSheet.create({
  container: {
    width: '100%',
    backgroundColor: COLOR.FooterBackground,
    height: 44,
    alignItems: "center",
    position: 'absolute',
    bottom: 0
  },
  text: {
    lineHeight: 34,
    fontSize: 10,
    color: COLOR.FooterFontColor
  }
});

function formatYYYYMMDD(d){
    var month = '' + (d.getMonth() + 1);
    var day = '' + d.getDate();
    var year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
};

function formatAMPM(date) {
  var hours = date.getHours();
  var minutes = date.getMinutes();
  var ampm = hours >= 12 ? 'PM' : 'AM';
  hours = hours % 12;
  hours = hours ? hours : 12;
  minutes = minutes < 10 ? '0'+minutes : minutes;
  var strTime = hours + ':' + minutes + ' ' + ampm;
  var cur_day = formatYYYYMMDD(new Date());
  if(cur_day != formatYYYYMMDD(date)){
    strTime = cur_day + ' ' + strTime;
  }
  return strTime;
};



const INPUT_H = (isIphoneX()?130:130);
const INPUT_BOTTOM = (isIphoneX()?55:45);

class DialogBox extends Component {
  ds: Object;
  socket: Object;
  state: Object;
  chatListView: Object;

  _reachEnd = true;
  _userHasInput: boolean = false;

  constructor(props: Object) {
    super(props);
    this.ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => {
        return r1 != r2;
      }
    })

    this.state = {
      inputValue: '',
      firstTime: true,
      refreshing: false,
      userId: this.props.userId,
      userInfo: this.props.userInfo,
      messageHistory: [],
      serviceName: this.props.serviceName,
    }

    let server = '';
    // console.log('dev ==>', this.props.dev)
    if(this.props.dev){
      server = 'wss://xuelvjiatest.wul.ai/v1/chat'
    }else{
      server = 'wss://xuelvjiasdk.wul.ai/v1/chat'
    }
    // console.log('init ws ==>', server);
    this._connect(server);
  }

  // componentWillMount () {
  //   this.keyboardWillShowSub = Keyboard.addListener('keyboardWillShow', this.keyboardWillShow);
  //   this.keyboardWillHideSub = Keyboard.addListener('keyboardWillHide', this.keyboardWillHide);
  // }

  // componentWillUnmount() {
  //   this.keyboardWillShowSub.remove();
  //   this.keyboardWillHideSub.remove();
  // }

  // keyboardWillShow = (event) => {
  //   console.log('keyboardWillShow')
  // };

  // keyboardWillHide = (event) => {
  //   console.log('keyboardWillHide')
  // };

  _connect(server) {
    this.socket = new WebSocket(server);

    this.socket.onopen = () => {
      // console.log('open ...')
      if(this.state.firstTime){
        // console.log('login ...')
        this._login();
      }
    }

    this.socket.onmessage = (e) => {
      let result = JSON.parse(e.data);
      // console.log(result);
      if(result.action === 'login' && result.ok){
        this.setState({
          messageHistory: result.history
        })
      }else{
        this.setState(prevState => ({
          messageHistory: [...prevState.messageHistory, {
            msg: result.data,
            time: new Date(),
            is_self: false
          }]
        }))
      };
    }

    this.socket.onerror = (e) => {
      console.log('onerror', e.message);
    }

    this.socket.onclose = (e) => {
      this.setState({
        'firstTime': false
      }, () => {
        this._connect(server);
      })
      // console.log('onclose');
    }
  }

  _scrollToBottom() {
    this.chatListView.scrollToEnd();
  }

  _onSubmit = () => {
    // console.log('submit')
    if(!this._checkConnect()){
      // console.log('disconnect from server ...');
      return;
    }

    let val = this.inputValue;
    if(!val || !val.length) {
      // console.log('no input')
      return;
    }
    if(val.length > 512){
      val = val.slice(0, 500) + '...';
    }
    this.socket.send(JSON.stringify({
      action: 'query',
      user_id: this.state.userId,
      msg: val,
      time: new Date(),
      is_self: true
    }));
    // console.log("send=>", val);
    this.setState(prevState => ({
      messageHistory: [...prevState.messageHistory, {
        msg: val,
        time: new Date(),
        is_self: true
      }],
    }))
    this.inputValue = '';

    // weird issue on ios https://github.com/facebook/react-native/issues/18272
    if (Platform.OS === 'ios') {
      this.inputElement.setNativeProps({ text: ' ' });
      setTimeout(() => {
        this.inputElement.setNativeProps({ text: '' });
      });
    } else {
      this.inputElement.clear();
    }
    console.log('clear...')
  }

  _login = (msg, t) => {
    this.socket.send(JSON.stringify({
      action: 'login',
      user_id: this.state.userId,
      user_info: JSON.parse(this.state.userInfo),
      device: Platform.OS
    }))
  }

  _checkConnect = () => {
    if(this.socket.readyState === 1){
      return true;
    }else{
      return false;
    }
  }

  _onPullMessage = () => {
      this.setState({
            refreshing: false
        });
  }

  _triggerInput = () => {
    this.inputElement.focus();
  }

  _renderRow = (row, sectionID, rowID) => {
    let t = formatAMPM(new Date(row.time));
    let time_style = {
      paddingTop: 18,
      paddingLeft: 28,
      paddingRight: 18
    };
    let dialog_style = {
      marginLeft: 13,
      marginRight: "25%",
      padding: 5
    };
    let arrow = {
      borderWidth: 6,
      height: 6,
      top: 10,
      marginLeft: -1,
      borderTopColor: "transparent",
      borderLeftColor: "#38b2a6",
      borderRightColor: "#38b2a6",
      borderBottomColor: "transparent"
    }
    let text_style = {
      padding: 9,
      fontSize: 12,
      borderRadius: 2,
      overflow: "hidden"
    }
    if(row.is_self) {
      time_style.flexDirection = 'row-reverse'
      dialog_style.flexDirection = 'row-reverse'
      arrow.borderLeftColor = '#38b2a6'
      arrow.borderRightColor = 'transparent'
      text_style.color = "#ffffff"
      text_style.backgroundColor = "#38b2a6"
    } else {
      t = this.state.serviceName + " " + t
      time_style.flexDirection = 'row'
      dialog_style.flexDirection = 'row'
      arrow.borderLeftColor = 'transparent'
      arrow.borderRightColor = '#f5f5f5'
      text_style.backgroundColor = "#f5f5f5"
    }
    return (
      <View>
        <View style={time_style}>
          <Text style={dialog_styles.time}>{ t }</Text>
        </View>
        <View style={dialog_style}>
          <View style={arrow}></View>
          <Text
            style={text_style}
            selectable={true}
            >
            { row.msg }
          </Text>
        </View>
      </View>
    )
  };

  render() {
    return (
      <View style={dialog_styles.container}>
        <ListView
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this._onPullMessage}
            />
          }
          onEndReached={() => {
            this._reachEnd = true;
          }}
          onEndReachedThreshold={10}
          ref={(reference) => { this.chatListView = reference; }}
          dataSource={this.ds.cloneWithRows(this.state.messageHistory.slice())}
          enableEmptySections={true}
          onLayout={
            (event) => {
              this._scrollToBottom();
            }
          }
          onContentSizeChange={
            (event) => {
              this._scrollToBottom();
            }
          }
          renderRow={this._renderRow}
        />
        <TouchableOpacity
          activeOpacity={1}
          onPress={
            () => this._triggerInput()
          }
          style={dialog_styles.inputView}
          >
          <View>
            <TextInput
              ref={(e) => { this.inputElement = e; }}
              multiline={true}
              controlled={true}
              placeholder="请输入..."
              underlineColorAndroid='transparent'
              enablesReturnKeyAutomatically={true}
              onChangeText={
                (text) => {
                  this.inputValue = text;
                }
              }
              />
            </View>
            <TouchableOpacity
              style={dialog_styles.sendButton}
              onPress={this._onSubmit}
              >
                <Text style={dialog_styles.buttonText}>发送</Text>
            </TouchableOpacity>
          </TouchableOpacity>
      </View>
    )
  }
};

const dialog_styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    backgroundColor: '#ffffff',
    bottom: 0
  },
  time: {
    fontSize: 10,
    fontWeight: "500",
    color: "#aeaeae"
  },
  inputView: {
    height: INPUT_H,
    borderTopColor: COLOR.BorderColor,
    borderTopWidth: 1,
    backgroundColor: "#ffffff",
    padding: 5
  },
  sendButton: {
    backgroundColor: COLOR.ButtonBackground,
    width: 52,
    height: 30,
    position: 'absolute',
    right: 15,
    bottom: INPUT_BOTTOM,
    borderRadius: 2
  },
  buttonText: {
    color: COLOR.ButtonFontColor,
    lineHeight: 30,
    textAlign: 'center'
  }
});


class IM extends Component {

  render() {
    let dev = this.props.dev;
    let userId = this.props.userId;
    let userInfo = this.props.userInfo;
    let serviceName = this.props.serviceName;
    let closeHandler = this.props.closeHandler;

    if(isIphone()){
      return (
        <KeyboardAvoidingView
          behavior="padding"
          style={styles.KeyboardAvoidingView}
          >
        <View style={styles.container}>
          <Header
              closeHandler={closeHandler}
              />

          <DialogBox
            dev={dev}
            userId={userId}
            userInfo={userInfo}
            serviceName={serviceName}
            closeHandler={closeHandler}
            />
          <Footer/>
        </View>
       </KeyboardAvoidingView>
      )
    } else {
      return (
        // <KeyboardAvoidingView
        //   behavior="padding"
        //   style={styles.KeyboardAvoidingView}
        //   >
        <SafeAreaView style={styles.container}>
          <Header
              closeHandler={closeHandler}
              />

          <DialogBox
            userId={userId}
            userInfo={userInfo}
            serviceName={serviceName}
            closeHandler={closeHandler}
            />
          <Footer/>
        </SafeAreaView>
        // </KeyboardAvoidingView>

      )
    }
  }
};

if(isIphone()){
  if(isIphoneX()){
    STATUS_HEIGHT = 44;
  }else{
    STATUS_HEIGHT = 20;
  }
}else{
  STATUS_HEIGHT = 0;
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLOR.HeaderBackground,
    paddingTop: STATUS_HEIGHT
  },
  KeyboardAvoidingView: {
    flex: 1,
  }
});

export default IM;

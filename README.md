### 吾来APP SDK 使用说明 ###

**注：此版本是React Native的SDK。请使用React Native 0.55以上版本（包括0.55）**

1. **加载**

    在项目引入IM.js
2. **使用方法**

```
class IMDemo extends React.Component {
  handleClose() {
    console.log('handle close');
  }

  render() {
    let userId = "test001";
    let pubkey = "XXX";
    let env = "dev";
    let async = false;
    let userInfo = JSON.stringify({
      "extra": "something"
    })
    let serviceName = "吾来IM";

    return (
      <IM
        dev={true}
        userId={userId}
        pubkey={pubkey}
        env={env}
        async={async}
        userInfo={userInfo}
        serviceName={serviceName}
        closeHandler={this.handleClose}
      >
      </IM>
    );
  }
}
```

参数名 | 说明 | 备注
- | :-: | -:
userId | 用户ID |
pubkey | 吾来平台key |
env | 正式或测试环境 | 使用吾来平台的测试或正式环境
async | 是否使用异步方式接入吾来平台 | true或false
userinfo | 用户信息 | 可传入额外的用户信息，可以不填
serviceName | 客服昵称 |
closeHandler | 关闭按钮回调处理函数 |


#### 版本迭代

* v1.0 第一个稳定版本

var path = require('path');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

module.exports = {
    entry: "./src/IM.js",
    output: {
        path: path.resolve(__dirname, './src'),
        filename: "IM.min.js"
    },
    loader: {
      test: /\.js$/,
      exclude: /node_modules/,
      loader: 'babel-loader',
    },
    plugins: [
      new UglifyJSPlugin()
    ]
};

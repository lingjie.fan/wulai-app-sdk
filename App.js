import React from 'react';
import {
  AppRegistry,
  Button,
  View
} from 'react-native';
import {createStackNavigator} from 'react-navigation'
import IM from './src/IM.js'


class IMDemo extends React.Component {
  handleClose() {
    console.log('handle close');
  }

  render() {
    let userId = "test001";
    let userInfo = JSON.stringify({
      "campaign": {
                "gclid": "gcl_id_123",
                "utm_source": "source_a",
                "utm_medium": "medium_b",
                "utm_campaign": "utm_campaign_c",
                "utm_term": "term_x",
                "utm_content": "content_y"
             }
    })
    let serviceName = "Amy";
    return (
      <IM
        userId={userId}
        userInfo={userInfo}
        serviceName={serviceName}
        closeHandler={this.handleClose}
      >
      </IM>
    );
  }
}

//class Home extends React.Component {
//  render() {
//    return (
//      <View>
//        <Button
//          onPress={() => { this.props.navigation.navigate('IM')}}
//          title='测试'
//        />
//      </View>
//    )
//  }
//}
//
//const demo = createStackNavigator({
//  IM: {screen: WuLaiIM, title: "IM"},
//  home: {screen: Home, title: "Home"},
//})

export default IMDemo;
